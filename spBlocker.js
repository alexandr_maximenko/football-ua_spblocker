var url = document.URL;
var mainMenu = ['news/', 'results/', 'groups/', 'teams/', 'video/'];
var months = ['января', 'февраля', 'марта'];

function isInMainMenu(href) {
    var result = false;
    mainMenu.forEach(function (entry) {
        if (href.endsWith(entry)) {
            result = true;
            return;
        }
    });
    return result;
}

function getNewsFromChampionsAndMapOnFootballUaPage(response) {
    var newsFeedList = response.getElementsByClassName("news_feed_list");
    var ul = document.createElement("ul");

    newsFeedList[0].childNodes.forEach(function (newsFeed) {
        if (newsFeed instanceof HTMLDivElement) {
            var li = document.createElement("li");
            li.setAttribute("class", "news-date");
            li.appendChild(document.createTextNode(newsFeed.innerHTML));
            ul.appendChild(li);
        } else if (newsFeed instanceof HTMLAnchorElement) {
            var newUrl = newsFeed.getAttribute("href");
            newUrl = newUrl.replace("https://champions.", "http://").replace("/news/", "/uefa/");
            
            var li = document.createElement("li");
            
            var div = document.createElement("div");

            var divText = document.createTextNode(newsFeed.childNodes[1].innerHTML);
            div.setAttribute("class", "time");
            div.appendChild(divText);
            
            var a = document.createElement("a");
            a.setAttribute("href", newUrl);
            var aText = document.createTextNode(newsFeed.childNodes[3].innerHTML);
            a.appendChild(aText);
            
            li.appendChild(div);
            li.appendChild(a);
            ul.appendChild(li);
        }
    });
    var newsFeedArticle = document.getElementsByClassName("news-feed")[1];
    newsFeedArticle.innerHTML = '';
    
    var championsLeagueTitle = document.createElement("h3");
    championsLeagueTitle.setAttribute("class", "feed-header rss");
    championsLeagueTitle.appendChild(document.createTextNode("Лига Чемпионов. Новости"));
    
    newsFeedArticle.appendChild(championsLeagueTitle);
    newsFeedArticle.appendChild(ul);
}

function getNewsFromEuro2020AndMapOnFootballUaPage(response) {
    var mainArticle = response.getElementsByClassName("post post-variant-1")[0].childNodes[1];
    var topNews = document.createElement("ul");
    
    var timeDiv = document.createElement("div");
    timeDiv.setAttribute("class", "time");
    var href = "https://football.ua" + mainArticle.getAttribute("href").replace("news/article", "uefa/");
    get(href, function(response) {
        timeDiv.appendChild(document.createTextNode(response.getElementsByClassName("date")[0].innerHTML.split(", ")[1]));
    }, true);
    
    var a = document.createElement("a");
    a.setAttribute("href", href);
    a.appendChild(document.createTextNode(mainArticle.getAttribute("title")));
    
    var li = document.createElement("li");
    li.appendChild(timeDiv);
    li.appendChild(a);
    
    var ul = document.createElement("ul");
    ul.appendChild(li);
    
    var euro2020Title = document.createElement("h3");
    euro2020Title.setAttribute("class", "feed-header rss");
    euro2020Title.appendChild(document.createTextNode("Евро-2020. Главное") );
    
    var article = document.createElement("article");
    article.setAttribute("class", "news-feed");
    article.appendChild(euro2020Title);
    article.appendChild(ul);
    
    var separator = document.createElement("div");
    separator.setAttribute("class", "separator");
    article.append(separator);
    
    var newsFeedArticles = document.getElementsByClassName("news-feed");
    newsFeedArticles[0].parentNode.insertBefore(article, newsFeedArticles[0]);
}

function getAllNewsFromEuro2020AndMapOnFootballUaPage(response) {
    var news = response.getElementsByClassName("row news-line");
    var ul = document.createElement("ul");
    var pivotDate = new Date();
    Array.from(news).forEach( function(item) {
        var timeDiv = document.createElement("div");
        timeDiv.setAttribute("class", "time");
        
        var newsItem = item.childNodes[1].childNodes[1];
        var href = "https://football.ua" + newsItem.getAttribute("href").replace("news/article", "uefa/").replace("video/article/", "uefa/");
        get(href, function(response) {
            var dateTime = response.getElementsByClassName("date")[0].innerHTML.split(", ");
            timeDiv.appendChild(document.createTextNode(dateTime[1]));
            
            var date = dateTime[0].split(" ");
            var year = date[2];
            var month = date[1];
            var day = date[0];
                
            if((year != pivotDate.getFullYear().toString(10)) || (months.indexOf(month) != pivotDate.getMonth().toString(10)) || (day != pivotDate.getDate().toString(10))) {
                var li = document.createElement("li");
                li.setAttribute("class", "news-date");
                li.appendChild(document.createTextNode(dateTime[0]));
                ul.appendChild(li);
                pivotDate = new Date(year, months.indexOf(month), day, 0, 0, 0, 0);
            }
        }, false);
        
        
        var a = document.createElement("a");
        a.setAttribute("href", href);
        a.appendChild(document.createTextNode(newsItem.getAttribute("title")));
        
        var li = document.createElement("li");
        li.appendChild(timeDiv);
        li.appendChild(a);
        
        ul.appendChild(li);
    }, true);
    
    var euro2020Title = document.createElement("h3");
    euro2020Title.setAttribute("class", "feed-header rss");
    euro2020Title.appendChild(document.createTextNode("Евро-2020. Новости") );
    
    var article = document.createElement("article");
    article.setAttribute("class", "news-feed");
    article.appendChild(euro2020Title);
    article.appendChild(ul);
    
    var separator = document.createElement("div");
    separator.setAttribute("class", "separator");
    article.append(separator);
    
    var newsFeedArticles = document.getElementsByClassName("news-feed");
    newsFeedArticles[1].parentNode.insertBefore(article, newsFeedArticles[1]);
}

function get(requestUrl, action, async) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            var response = document.createElement("html");
            response.innerHTML = this.responseText;
            action(response);
        }
    };

    xhttp.open("GET", requestUrl, async);
    xhttp.send();
}

if (url.startsWith("https://football.ua")) {
    var rubric = document.getElementsByClassName("rubric")[0];
    if (typeof rubric !== 'undefined') {
        if (rubric.innerHTML === "Лига Чемпионов. Новость") {
            var championsUrl = url.replace("https://football.ua", "https://champions.football.ua").replace("/uefa/", "/news/");
            get(championsUrl, getNewsFromChampionsAndMapOnFootballUaPage, true);
        } else if (rubric.innerHTML === "Евро-2020") {
            get("https://euro2020.football.ua/news", getNewsFromEuro2020AndMapOnFootballUaPage, true);
            get("https://euro2020.football.ua/news?all=1", getAllNewsFromEuro2020AndMapOnFootballUaPage, false);
        }
    }
    var anchors = document.getElementsByTagName("a");
    Array.from(anchors).forEach(function (anchor) {
        var href = anchor.getAttribute("href");
        if (href !== null) {
            if (href.includes("champions.football.ua") && !(href.endsWith("football.ua") || href.endsWith("football.ua/"))) {
                anchor.setAttribute("href", href.replace("champions.football.ua", "football.ua").replace("/news/", "/uefa/"));
            }

            if (href.includes("euro2020.football.ua") && !(href.endsWith("football.ua") || href.endsWith("football.ua/"))) {
                anchor.setAttribute("href", href.replace("euro2020.football.ua", "football.ua").replace("/news/article", "/uefa/"));
            }
          
            if (href.startsWith("https://football.ua/euro2020/")) {
                anchor.setAttribute("href", href.replace("https://football.ua/euro2020/", "https://football.ua/uefa/"));
            }
        }
    });
}

if (url.startsWith("https://euro2020.football.ua")) {
    var anchors = document.getElementsByTagName("a");
    Array.from(anchors).forEach(function (anchor) {
        var href = anchor.getAttribute("href");
        if (href.startsWith("/news/article/")) {
            anchor.setAttribute("href", href.replace("/news/article/", "https://football.ua/uefa/"));
        }
    });
}

if (url.startsWith("https://champions.football.ua")) {
    var anchors = document.getElementsByTagName("a");
    Array.from(anchors).forEach(function (anchor) {
        var href = anchor.getAttribute("href");
        if (href.startsWith("https://champions.football.ua") && !isInMainMenu(href)) {
            href = href.replace("champions.football.ua", "football.ua");
            if (href.includes("/news")) {
                href = href.replace("/news/", "/uefa/");
            }
            if (href.includes("/galleries")) {
                if (href.endsWith("galleries") || href.endsWith("galleries/")) {
                    href = href.replace("football.ua", "champions.football.ua");
                } else {
                    href = href.replace("/galleries", "/gallery");
                }
            }
            
            anchor.setAttribute("href", href);
        }
    });
}